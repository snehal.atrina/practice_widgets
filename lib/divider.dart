import 'package:flutter/material.dart';

class My_divider extends StatefulWidget {
  const My_divider({Key? key}) : super(key: key);

  @override
  _My_dividerState createState() => _My_dividerState();
}

class _My_dividerState extends State<My_divider> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Divider',
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(Icons.bubble_chart),
          Divider(
            height: 30,
            color: Colors.red,
            thickness: 4,
            indent: 20,
            endIndent: 20,
          ),
          Icon(Icons.light),
          Divider(
            height: 30,
            color: Colors.red,
          ),
          Icon(Icons.light),
          Divider(
            height: 30,
            color: Colors.red,
          ),
          Icon(Icons.light),
          Divider(
            height: 30,
            color: Colors.red,
          ),
          Icon(Icons.light),
          Divider(
            height: 30,
            color: Colors.red,
          ),
          SizedBox(
            height: 30,
          ),
          Row(
            children: [
              Icon(Icons.light),
              VerticalDivider(
                width: 35,
                thickness: 2,
                color: Colors.deepPurple,
              ),
              Icon(Icons.bubble_chart),
            ],
          )
        ],
      ),
    );
  }
}
