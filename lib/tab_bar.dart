import 'package:flutter/material.dart';

class MyTabbar extends StatefulWidget {
  const MyTabbar({Key? key}) : super(key: key);

  @override
  _MyTabbarState createState() => _MyTabbarState();
}

class _MyTabbarState extends State<MyTabbar> {
  @override
  Widget build(BuildContext context) => DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            title: Text('TabBar'),
            centerTitle: true,
            bottom: const TabBar(
              automaticIndicatorColorAdjustment: true,
              enableFeedback: true,
              indicatorColor: Colors.red,
              // isScrollable: true,
              labelColor: Colors.orange,
              labelPadding: EdgeInsets.all(10),
              labelStyle: TextStyle(fontWeight: FontWeight.bold),
              padding: EdgeInsets.all(5),
              physics: ScrollPhysics(),
              unselectedLabelColor: Colors.deepOrange,
              unselectedLabelStyle: TextStyle(),
              //mouseCursor: SystemMouseCursors.text,
              tabs: [
                Tab(
                  text: 'Tab 1',
                  icon: Icon(Icons.home),
                ),
                Tab(
                  text: 'Tab 2',
                  icon: Icon(Icons.star),
                ),
                Tab(
                  text: 'Tab 3',
                  icon: Icon(Icons.person),
                ),
              ],
            ),
          ),
        ),
      );
}
