import 'package:flutter/material.dart';

class MyDropDown extends StatefulWidget {
  const MyDropDown({Key? key}) : super(key: key);

  @override
  _MyDropDownState createState() => _MyDropDownState();
}

class _MyDropDownState extends State<MyDropDown> {
  String selectedvalues = 'Orange';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dropdown List'),
      ),
      body: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Container(
          margin: EdgeInsets.all(10),
          height: 70,
          color: Colors.white60,
          width: MediaQuery.of(context).size.width,
          child: DropdownButton<String>(
            dropdownColor: Colors.lightBlueAccent,
            isExpanded: true,
            value: selectedvalues,
            icon: Icon(Icons.arrow_downward_rounded),
            onChanged: (String? newvalue) {
              setState(() {
                selectedvalues = newvalue!;
              });
            },
            items: ['Orange', 'Apple', 'Mango', 'Bannana', 'Grapes']
                .map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(value: value, child: Text(value));
            }).toList(),
          ),
        )
      ]),
    );
  }
}
