import 'package:flutter/material.dart';

class My_Gridview extends StatefulWidget {
  const My_Gridview({Key? key}) : super(key: key);

  @override
  _My_GridviewState createState() => _My_GridviewState();
}

class _My_GridviewState extends State<My_Gridview> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GridView.count(
        //scrollDirection: Axis.horizontal,
        crossAxisCount: 2,
        children: [
          Container(
            color: Colors.blue,
            margin: EdgeInsets.all(5),
          ),
          Container(
            color: Colors.blue,
            margin: EdgeInsets.all(5),
          ),
          Container(
            color: Colors.blue,
            margin: EdgeInsets.all(5),
          ),
          Container(
            color: Colors.blue,
            margin: EdgeInsets.all(5),
          ),
          Container(
            color: Colors.blue,
            margin: EdgeInsets.all(5),
          ),
          Container(
            color: Colors.blue,
            margin: EdgeInsets.all(5),
          ),
          Container(
            color: Colors.blue,
            margin: EdgeInsets.all(5),
          ),
          Container(
            color: Colors.blue,
            margin: EdgeInsets.all(5),
          ),
          Container(
            color: Colors.blue,
            margin: EdgeInsets.all(5),
          ),
          Container(
            color: Colors.blue,
            margin: EdgeInsets.all(5),
          ),
          Container(
            color: Colors.blue,
            margin: EdgeInsets.all(5),
          ),
          Container(
            color: Colors.blue,
            margin: EdgeInsets.all(5),
          ),
        ],
      ),
    );

    /*Scaffold(
      body: GridView.builder(
        itemCount: 16,
        gridDelegate:
            SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        itemBuilder: (ctx, index) {
          return Container(
            color: Colors.deepPurple,
            margin: EdgeInsets.all(5),
            child: Center(
              child: Text(
                index.toString(),
                style: TextStyle(
                    fontSize: 30,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),
            ),
          );
        },
      ),
    );*/

    /*Scaffold(
      body: GridView(
        gridDelegate:
            SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        children: [
          Container(
            color: Colors.blue,
            margin: EdgeInsets.all(5),
          ),
          Container(
            color: Colors.blue,
            margin: EdgeInsets.all(5),
          ),
          Container(
            color: Colors.blue,
            margin: EdgeInsets.all(5),
          ),
          Container(
            color: Colors.blue,
            margin: EdgeInsets.all(5),
          ),
          Container(
            color: Colors.blue,
            margin: EdgeInsets.all(5),
          ),
          Container(
            color: Colors.blue,
            margin: EdgeInsets.all(5),
          ),
          Container(
            color: Colors.blue,
            margin: EdgeInsets.all(5),
          ),
          Container(
            color: Colors.blue,
            margin: EdgeInsets.all(5),
          ),
          Container(
            color: Colors.blue,
            margin: EdgeInsets.all(5),
          ),
          Container(
            color: Colors.blue,
            margin: EdgeInsets.all(5),
          ),
        ],
      ),
    );*/
  }
}
