import 'package:flutter/material.dart';

class My_Linear_Progress extends StatefulWidget {
  @override
  _My_Linear_ProgressState createState() => _My_Linear_ProgressState();
}

class _My_Linear_ProgressState extends State<My_Linear_Progress> {
  @override
  Widget build(BuildContext context) {
    return Container(
        alignment: Alignment.topCenter,
        margin: EdgeInsets.only(top: 20),
        child: Column(children: [
          Container(
            child: Text(
              "Progress Indicator",
              style: TextStyle(fontSize: 18),
            ),
            margin: EdgeInsets.all(20),
          ),
          Container(
            margin: EdgeInsets.all(20),
            child: LinearProgressIndicator(
              backgroundColor: Colors.redAccent,
              valueColor: AlwaysStoppedAnimation(Colors.green),
              minHeight: 20,
            ),
          ),
        ]));
  }
}
