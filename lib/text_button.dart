import 'package:flutter/material.dart';

class My_Text_Button extends StatefulWidget {
  const My_Text_Button({Key? key}) : super(key: key);

  @override
  _My_Text_ButtonState createState() => _My_Text_ButtonState();
}

class _My_Text_ButtonState extends State<My_Text_Button> {
  @override
  Widget build(BuildContext context) {
    var Fluttertoast;
    return Scaffold(
      appBar: AppBar(
        title: Text('Text_Button'),
      ),
      body: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(30),
        child: TextButton.icon(
          label: Text('Text Button'),
          icon: Icon(Icons.settings),
          onPressed: () => Fluttertoast.showToast(
            msg: 'Pressed Icon Button',
            fontSize: 18,
          ),
        ),
      ),
    );
  }
}
