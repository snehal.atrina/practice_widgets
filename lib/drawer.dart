import 'package:flutter/material.dart';

class MyDrawer extends StatefulWidget {
  const MyDrawer({Key? key}) : super(key: key);
  @override
  State<MyDrawer> createState() => _MyDrawerState();
}

class _MyDrawerState extends State<MyDrawer> {
  @override
  Widget build(BuildContext context) => Drawer(
        child: Material(
          color: Color.fromRGBO(50, 75, 205, 1),
          child: ListView(
            children: [
              SizedBox(height: 45),
              buildMenuItem(text: 'People', icon: Icons.people),
              SizedBox(height: 45),
              buildMenuItem(text: 'Favorite', icon: Icons.favorite),
              SizedBox(height: 45),
              buildMenuItem(text: 'Workflow', icon: Icons.workspaces_outline),
              SizedBox(height: 45),
              buildMenuItem(text: 'Updates', icon: Icons.update),
            ],
          ),
        ),
      );

  buildMenuItem({
    required String text,
    required IconData icon,
  }) {
    final color = Colors.white;
    final hoverColor = Colors.white70;

    return ListTile(
      leading: Icon(icon, color: color),
      title: Text(text, style: TextStyle(color: color)),
      hoverColor: hoverColor,
      onTap: () {},
    );
  }
}
