import 'package:flutter/material.dart';

class My_Listtile extends StatefulWidget {
  const My_Listtile({Key? key}) : super(key: key);

  @override
  _My_ListtileState createState() => _My_ListtileState();
}

class _My_ListtileState extends State<My_Listtile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('List Tile'),
      ),
      body: ListView(
        children: [
          ListTile(
            title: Text('List Tile'),
            subtitle: Text('List Subtitle'),
            leading: Icon(Icons.person),
            trailing: Icon(Icons.star),
            onTap: () {},
            onLongPress: () {},
            // dense: true,
            isThreeLine: true,
            tileColor: Colors.blueGrey,
            //enabled: false,
            //contentPadding: EdgeInsets.all(30),
          ),
          ListTile(
            title: Text('List Tile'),
            subtitle: Text('List Subtitle'),
            leading: Icon(Icons.person),
            trailing: Icon(Icons.star),
            selected: true,
            selectedTileColor: Colors.blueGrey,
            // contentPadding: EdgeInsets.all(30),
          ),
        ],
      ),
    );
  }
}
