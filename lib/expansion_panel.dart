import 'package:flutter/material.dart';

class My_Expansion_Panel extends StatefulWidget {
  const My_Expansion_Panel({Key? key}) : super(key: key);

  @override
  _My_Expansion_PanelState createState() => _My_Expansion_PanelState();
}

class _My_Expansion_PanelState extends State<My_Expansion_Panel> {
  List<bool> _isExpanded = List.generate(10, (_) => false);
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: ExpansionPanelList(
          expansionCallback: (index, isExpanded) => setState(() {
            _isExpanded[index] = !isExpanded;
          }),
          children: [
            for (int i = 0; i < 10; i++)
              ExpansionPanel(
                body: const ListTile(
                    subtitle: Text(
                        "... “People tell you the world looks a certain way. Parents tell you how to think. Schools tell you how to think. TV. Religion. And then at a certain point, if you’re lucky, you realize you can make up your own mind. Nobody sets the rules but you. You can design your own life.”")),
                headerBuilder: (_, isExpanded) {
                  return const Center(
                    child: Text("Carrie Ann Moss Inspiration ..."),
                  );
                },
                isExpanded: _isExpanded[i],
              )
          ],
        ),
      ),
    );
  }
}
