import 'package:flutter/material.dart';

class My_Tooltip extends StatefulWidget {
  const My_Tooltip({Key? key}) : super(key: key);

  @override
  _My_TooltipState createState() => _My_TooltipState();
}

class _My_TooltipState extends State<My_Tooltip> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ToolTip'),
      ),
      body: Center(
        child: Tooltip(
          preferBelow: false,
          height: 50,
          showDuration: Duration(seconds: 2),
          verticalOffset: 50,
          textStyle: TextStyle(color: Colors.red),
          message: 'Update Text',
          child: ElevatedButton(child: Icon(Icons.update), onPressed: () {}),
        ),
      ),
    );
  }
}
