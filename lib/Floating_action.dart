import 'package:flutter/material.dart';

class My_Floating extends StatefulWidget {
  @override
  _My_FloatingState createState() => _My_FloatingState();
}

class _My_FloatingState extends State<My_Floating> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Floating Action button'),
      ),
      body: Center(
        child: Text('Floating Action Button'),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.lightBlue,
        autofocus: true,
        focusNode: FocusNode(canRequestFocus: true),
        focusElevation: 10,
        hoverColor: Colors.red,
        elevation: 50,
        highlightElevation: 50,
        splashColor: Colors.lightGreenAccent,
        foregroundColor: Colors.orange,
        child: Icon(Icons.add),
        onPressed: () {},
        shape: RoundedRectangleBorder(
            side: BorderSide(
              width: 2,
              color: Colors.black,
            ),
            borderRadius: BorderRadius.all(Radius.circular(20))),
      ),
    );
  }
}
