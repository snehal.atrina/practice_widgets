import 'package:flutter/material.dart';

class My_Bottom_Sheet extends StatefulWidget {
  const My_Bottom_Sheet({Key? key}) : super(key: key);

  @override
  _My_Bottom_SheetState createState() => _My_Bottom_SheetState();
}

class _My_Bottom_SheetState extends State<My_Bottom_Sheet> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Bottom Sheet',
          style: TextStyle(color: Colors.red),
        ),
      ),
      body: Center(
        child: ElevatedButton(
          child: Text('Show Bottom Sheet'),
          onPressed: () {
            showModalBottomSheet(
                backgroundColor: Theme.of(context).canvasColor,
                elevation: 20,
                isDismissible: true,
                enableDrag: false,
                isScrollControlled: true,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20),
                  ),
                ),
                context: context,
                builder: (context) {
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      ListTile(
                        title: Text('Orange'),
                        subtitle: Text('Karan'),
                      ),
                      ListTile(
                        title: Text('Apple'),
                        subtitle: Text('Akshay'),
                      ),
                      ListTile(
                        title: Text('Bannana'),
                        subtitle: Text('kaya'),
                      ),
                      ListTile(
                        title: Text('Mango'),
                        subtitle: Text('Snehal'),
                      ),
                      ListTile(
                        title: Text('Berry'),
                        subtitle: Text('Karan'),
                      ),
                      ListTile(
                        title: Text('Grapes'),
                        subtitle: Text('Payal'),
                      ),
                      ListTile(
                        title: Text('Grapes'),
                        subtitle: Text('Payal'),
                      ),
                    ],
                  );
                });
          },
        ),
      ),
    );
  }
}
