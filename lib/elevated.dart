import 'package:flutter/material.dart';

class MyElevated extends StatefulWidget {
  @override
  _MyElevatedState createState() => _MyElevatedState();
}

class _MyElevatedState extends State<MyElevated> {
  @override
  Widget build(BuildContext context) {
    var Fluttertoast;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Elevated Button'),
      ),
      body: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(30),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              minimumSize: Size(300, 40),
              textStyle: TextStyle(fontSize: 30),
              primary: Colors.deepOrange,
              onPrimary: Colors.black),
          child: Text('Elevated Button'),
          onPressed: () => Fluttertoast.showToast(
            msg: 'Pressed Elevated Button',
            fontSize: 20,
          ),
        ),
      ),
      /* body: Center(
        child: Center(
          child: ConstrainedBox(
            constraints: const BoxConstraints.tightFor(height: 150, width: 150),
            child: ElevatedButton(
              child: Text('Button'),
              onPressed: () {},
              autofocus: true,
              style: ElevatedButton.styleFrom(
                  shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(
                        (10),
                      ),
                    ),
                  ),
                  primary: Colors.blue,
                  textStyle: const TextStyle(
                      fontSize: 25, fontStyle: FontStyle.italic),
                  elevation: 25,
                  shadowColor: Colors.orange,
                  side: const BorderSide(color: Colors.black)),
            ),
            /* child: ElevatedButton.icon(
              onPressed: () {},
              icon: Icon(Icons.bubble_chart),
              label: Text('Button')),*/

            /* child: ElevatedButton.(
            child: Text('Button'),
            onPressed: () {
              print('press');
            },
            onLongPress: () {
              print('Long Press');
            },
          ),*/
          ),
        ),
      ),*/
    );
  }
}
