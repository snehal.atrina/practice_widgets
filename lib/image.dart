import 'package:flutter/material.dart';

class My_Image extends StatefulWidget {
  const My_Image({Key? key}) : super(key: key);

  @override
  _My_ImageState createState() => _My_ImageState();
}

class _My_ImageState extends State<My_Image> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Image'),
      ),
      body: Center(
        child: Column(children: [
          Image.asset(
            'Asset/Images/rain.jpg',
            height: 200,
            scale: 2,
            opacity: AlwaysStoppedAnimation<double>(0.5),
            alignment: Alignment.center,
          ),
        ]),
      ),
    );
  }
}
