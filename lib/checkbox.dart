import 'package:flutter/material.dart';

class My_Checkbox extends StatefulWidget {
  const My_Checkbox({Key? key}) : super(key: key);

  @override
  _My_CheckboxState createState() => _My_CheckboxState();
}

class _My_CheckboxState extends State<My_Checkbox> {
  bool value = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('CheckBox'),
      ),
      body: ListView(
        children: [
          buildCheckbox(),
        ],
      ),
    );
  }

  buildCheckbox() => ListTile(
        onTap: () {
          setState(() {
            this.value = !value;
          });
        },
        leading: Checkbox(
          value: value,
          onChanged: (value) {
            setState(() {
              this.value = value!;
            });
          },
        ),
        title: Text(
          'Click Me!',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.w100),
        ),
      );
}
