import 'package:flutter/material.dart';

class My_TextField extends StatefulWidget {
  const My_TextField({Key? key}) : super(key: key);

  @override
  _My_TextFieldState createState() => _My_TextFieldState();
}

class _My_TextFieldState extends State<My_TextField> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 40),
        child: Center(
          child: TextField(
            autofocus: true,
            //enabled: false,
            // keyboardType: TextInputType.text,
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.orange,
              hintText: 'Name',
              hintStyle: TextStyle(color: Colors.amber),
              helperText: 'or username',
              labelText: "First Name",
              // border: InputBorder.none,
              border: OutlineInputBorder(),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.redAccent)),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.purpleAccent)),
            ),
          ),
        ),
      ),
    );
  }
}
