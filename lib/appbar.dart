import 'package:flutter/material.dart';

class MyAppBar extends StatefulWidget {
  const MyAppBar({Key? key}) : super(key: key);
  @override
  State<MyAppBar> createState() => _MyAppBarState();
}

class _MyAppBarState extends State<MyAppBar> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'AppBar',
          style: TextStyle(fontSize: 25, fontStyle: FontStyle.italic),
        ),
        centerTitle: true,
        //backgroundColor: Colors.purpleAccent,
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [Colors.blue, Colors.pinkAccent, Colors.purple])),
        ),

        elevation: 20,
        titleSpacing: 0,
        actions: [
          IconButton(
            icon: Icon(Icons.notifications_none),
            onPressed: () => {},
          ),
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () => {},
          ),
        ],
        leading: Icon(Icons.menu),
      ),
    );
  }
}
