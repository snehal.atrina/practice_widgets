import 'package:flutter/material.dart';

class MyTabBarView extends StatefulWidget {
  const MyTabBarView({Key? key}) : super(key: key);

  @override
  _MyTabBarViewState createState() => _MyTabBarViewState();
}

class _MyTabBarViewState extends State<MyTabBarView> {
  @override
  Widget build(BuildContext context) => DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            title: Text('TabBar'),
            centerTitle: true,
            bottom: const TabBar(
              automaticIndicatorColorAdjustment: true,
              enableFeedback: true,
              indicatorColor: Colors.red,
              // isScrollable: true,
              labelColor: Colors.orange,
              labelPadding: EdgeInsets.all(10),
              labelStyle: TextStyle(fontWeight: FontWeight.bold),
              padding: EdgeInsets.all(5),
              physics: ScrollPhysics(),
              unselectedLabelColor: Colors.deepOrange,
              unselectedLabelStyle: TextStyle(),
              //mouseCursor: SystemMouseCursors.text,
              tabs: [
                Tab(
                  text: 'Tab 1',
                  icon: Icon(Icons.home),
                ),
                Tab(
                  text: 'Tab 2',
                  icon: Icon(Icons.star),
                ),
                Tab(
                  text: 'Tab 3',
                  icon: Icon(Icons.person),
                ),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              Center(
                child: Text('Tab 1 Content'),
              ),
              Center(
                child: Text('Tab 2 Content'),
              ),
              Center(
                child: Text('Tab 3 Content'),
              ),
            ],
          ),
        ),
      );
}
