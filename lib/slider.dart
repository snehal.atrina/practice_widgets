import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class My_Slider extends StatefulWidget {
  const My_Slider({Key? key}) : super(key: key);

  @override
  _My_SliderState createState() => _My_SliderState();
}

class _My_SliderState extends State<My_Slider> {
  double value = 50;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Slider',
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Slider(
            value: value,
            thumbColor: Colors.cyanAccent,
            min: 0,
            max: 100,
            divisions: 100,
            activeColor: Colors.red,
            inactiveColor: Colors.red.shade100,
            label: value.round().toString(),
            onChanged: (value) => setState(
              () => this.value = value,
            ),
          ),
        ],
      ),
    );
  }
}
