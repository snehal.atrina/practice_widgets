import 'package:flutter/material.dart';

import 'Constants.dart';

class My_PopupMenuButton extends StatefulWidget {
  @override
  _My_PopupMenuButtonState createState() => _My_PopupMenuButtonState();
}

class _My_PopupMenuButtonState extends State<My_PopupMenuButton> {
  @override
  var selected = "Body";
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        actions: [
          PopupMenuButton<String>(
            onSelected: choiceAction,
            itemBuilder: (BuildContext context) {
              return Constants.choices.map((String choice) {
                return PopupMenuItem<String>(
                  value: choice,
                  child: Text(choice),
                );
              }).toList();
            },
          )
        ],
      ),
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            new Text(
              selected,
            ),
          ],
        ),
      ),
    );
  }

  void choiceAction(String choice) {
    if (choice == Constants.Settings) {
      setState(() {
        selected = 'Settings';
      });
    } else if (choice == Constants.Subscribe) {
      setState(() {
        selected = 'Subscribe';
      });
      print('Subscribe');
    } else if (choice == Constants.SignOut) {
      setState(() {
        selected = 'SignOut';
      });
      print('SignOut');
    }
  }
}
