import 'package:flutter/material.dart';

class My_Outlined_Button extends StatefulWidget {
  const My_Outlined_Button({Key? key}) : super(key: key);

  @override
  _My_Outlined_ButtonState createState() => _My_Outlined_ButtonState();
}

class _My_Outlined_ButtonState extends State<My_Outlined_Button> {
  @override
  Widget build(BuildContext context) {
    var Fluttertoast;
    return Scaffold(
        appBar: AppBar(
          title: Text('Outlined_Button'),
        ),
        body: Center(
          child: TextButton(
            onPressed: () {},
            child: Text('TextButton'),
            style: TextButton.styleFrom(
              backgroundColor: Colors.lightBlueAccent,
              primary: Colors.lightGreen,
              padding: EdgeInsets.all(10),
              minimumSize: Size(150, 150),
            ),
          ),
        ));
  }
}
