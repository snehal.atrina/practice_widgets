import 'package:flutter/material.dart';

class My_Chip extends StatefulWidget {
  @override
  _My_ChipState createState() => _My_ChipState();
}

class _My_ChipState extends State<My_Chip> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Chip Widget'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Chip Widget'),
              Wrap(
                spacing: 10,
                children: const [
                  Chip(
                    shadowColor: Colors.blue,
                    backgroundColor: Colors.grey,
                    avatar: CircleAvatar(
                      child: Text('F'),
                    ),
                    label: Text(
                      "First",
                      style: TextStyle(color: Colors.orange),
                    ),
                  ),
                  Chip(
                    avatar: CircleAvatar(
                      child: Text('S'),
                    ),
                    label: Text(
                      "Second",
                      style: TextStyle(color: Colors.orange),
                    ),
                  ),
                ],
              )
            ],
          ),
        ));
  }
}
