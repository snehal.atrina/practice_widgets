import 'package:flutter/material.dart';

import 'drawer.dart';

class scaf extends StatefulWidget {
  @override
  scafState createState() => scafState();
}

class scafState extends State<scaf> {
  int currentIndex = 0;
  final screens = [
    Center(
      child: Icon(
        Icons.home,
        color: Colors.lightBlueAccent,
        size: 100,
      ),
    ),
    Center(
      child: Icon(
        Icons.favorite,
        color: Colors.redAccent,
        size: 100,
      ),
    ),
    Center(
      child: Icon(
        Icons.chat,
        color: Colors.orange,
        size: 100,
      ),
    ),
    Center(
      child: Icon(
        Icons.person,
        color: Colors.purple,
        size: 100,
      ),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'AppBar',
          style: TextStyle(fontSize: 25, fontStyle: FontStyle.italic),
        ),
        centerTitle: true,
        //backgroundColor: Colors.purpleAccent,
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [Colors.blue, Colors.pinkAccent, Colors.purple])),
        ),

        elevation: 20,
        titleSpacing: 0,
        actions: [
          IconButton(
            icon: Icon(Icons.notifications_none),
            onPressed: () => {},
          ),
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () => {},
          ),
        ],
      ),
      body: screens[currentIndex],
      drawer: MyDrawer(),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: currentIndex,
        elevation: 10,
        backgroundColor: Colors.lightBlueAccent,
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.black45,
        iconSize: 30,
        selectedLabelStyle: TextStyle(color: Colors.black38),
        unselectedLabelStyle: TextStyle(color: Colors.green),
        onTap: (index) {
          setState(() {
            currentIndex = index;
          });
        },
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
              color: Colors.white,
            ),
            label: 'Home',
            backgroundColor: Colors.lightBlueAccent,
          ),
          BottomNavigationBarItem(
            label: 'Feed',
            icon: Icon(
              Icons.favorite,
              color: Colors.white,
            ),
            backgroundColor: Colors.redAccent,
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.chat,
              color: Colors.white,
            ),
            label: 'Chat',
            backgroundColor: Colors.orange,
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.person,
              color: Colors.white,
            ),
            label: 'Profile',
            backgroundColor: Colors.purple,
          ),
        ],
      ),
    );
  }
}
