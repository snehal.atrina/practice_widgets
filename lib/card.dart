import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class My_card extends StatefulWidget {
  const My_card({Key? key}) : super(key: key);

  @override
  _My_cardState createState() => _My_cardState();
}

class _My_cardState extends State<My_card> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Card'),
      ),
      body: Container(
        height: 200,
        width: 200,
        child: Card(
          color: Colors.white60,
          shadowColor: Colors.amber,
          elevation: 50.0,
          margin: EdgeInsets.all(10),
          //shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50.0)),
        ),
      ),
    );
  }
}
