import 'package:flutter/material.dart';

class MySliverAppBar extends StatefulWidget {
  const MySliverAppBar({Key? key}) : super(key: key);

  @override
  _MySliverAppBarState createState() => _MySliverAppBarState();
}

class _MySliverAppBarState extends State<MySliverAppBar> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
          headerSliverBuilder: (BuildContext context,
                  bool innerBoxIsScrolled) =>
              [
                SliverAppBar(
                  foregroundColor: Colors.lightBlue,
                  elevation: 50,
                  leading: Icon(Icons.menu),
                  shape: RoundedRectangleBorder(
                      borderRadius:
                          BorderRadius.vertical(bottom: Radius.circular(30))),
                  backgroundColor: Colors.lightGreenAccent,
                  toolbarHeight: 60,
                  pinned: true,
                  shadowColor: Colors.red,
                  stretchTriggerOffset: 20,
                  title: Text(" Sliver App Bar Demo"),
                ),
              ],
          body: ListView.separated(
            padding: EdgeInsets.all(10),
            itemCount: 30,
            itemBuilder: (BuildContext context, int index) => ListTile(
              title: Text('Item $index'),
            ),
            separatorBuilder: (BuildContext context, int index) =>
                Divider(color: Colors.black),
          )),
    );
  }
}
