import 'package:flutter/material.dart';

class My_Simple_Dialog extends StatefulWidget {
  const My_Simple_Dialog({Key? key}) : super(key: key);

  @override
  _My_Simple_DialogState createState() => _My_Simple_DialogState();
}

class _My_Simple_DialogState extends State<My_Simple_Dialog> {
  var _selected = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Simple Dialog"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                _displayDialog(context);
              },
              child: Text("Show Dialog"),
            ),
            Text(_selected)
          ],
        ),
      ),
    );
  }

  _displayDialog(BuildContext context) async {
    _selected = await showDialog(
      context: context,
      builder: (BuildContext context) {
        return Expanded(
          child: SimpleDialog(
            title: Text('Choose food'),
            children: [
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, "Pizza");
                },
                child: const Text('Pizza'),
              ),
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, "Burger");
                },
                child: const Text('Burger'),
              ),
            ],
            elevation: 10,
            //backgroundColor: Colors.green,
          ),
        );
      },
    );

    if (_selected != null) {
      setState(() {
        _selected = _selected;
      });
    }
  }
}
