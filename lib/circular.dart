import 'package:flutter/material.dart';

class My_Circular_Indicator extends StatefulWidget {
  const My_Circular_Indicator({Key? key}) : super(key: key);

  @override
  _My_Circular_IndicatorState createState() => _My_Circular_IndicatorState();
}

class _My_Circular_IndicatorState extends State<My_Circular_Indicator> {
  @override
  Widget build(BuildContext context) {
    return Container(
        alignment: Alignment.topCenter,
        margin: EdgeInsets.only(top: 20),
        child: Column(
            children: [
          Container(
            child: Text(
              "Progress Indicator",
              style: TextStyle(fontSize: 18),
            ),
            margin: EdgeInsets.all(20),
          ),
          Container(
            margin: EdgeInsets.all(20),
            child: CircularProgressIndicator(
              backgroundColor: Colors.grey,
              color: Colors.blue,
              strokeWidth: 5,
            ),
          )
        ]));
  }
}
