import 'package:flutter/material.dart';

class My_icon extends StatefulWidget {
  const My_icon({Key? key}) : super(key: key);

  @override
  _My_iconState createState() => _My_iconState();
}

class _My_iconState extends State<My_icon> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Icon',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      body: Center(
        child: Center(
          child: Icon(
            Icons.bubble_chart,
            size: 100,
            color: Colors.red,
            semanticLabel: 'Bubble',
            //textDirection: TextDirection.rtl,
          ),
        ),
      ),
    );
  }
}
