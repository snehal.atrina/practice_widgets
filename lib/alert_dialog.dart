import 'package:flutter/material.dart';

class My_Alert_Dialog extends StatefulWidget {
  const My_Alert_Dialog({Key? key}) : super(key: key);

  @override
  _My_Alert_DialogState createState() => _My_Alert_DialogState();
}

class _My_Alert_DialogState extends State<My_Alert_Dialog> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Alert Dialog'),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            showDialog(
              context: context,
              builder: (context) => AlertDialog(
                //titlePadding:
                // const EdgeInsets.symmetric(vertical: 50, horizontal: 10),
                elevation: 20,
                titleTextStyle:
                    TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                title: Text('Alert Dialog Opened'),
                content: Text('Do you want to close alert Dialog Box ?'),
                actions: [
                  TextButton(
                      onPressed: () {
                        print('Yes is selected');
                        Navigator.pop(context);
                      },
                      child: Text('Yes')),
                  TextButton(
                      onPressed: () {
                        print('No is selected');
                        Navigator.pop(context);
                      },
                      child: Text('No')),
                ],
              ),
            );
          },
          child: Text('Open Alert Dialog'),
        ),
      ),
    );
  }
}
