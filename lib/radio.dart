import 'package:flutter/material.dart';

class My_Radio extends StatefulWidget {
  const My_Radio({Key? key}) : super(key: key);

  @override
  _My_RadioState createState() => _My_RadioState();
}

class _My_RadioState extends State<My_Radio> {
  int _value = -1;
  @override
  Widget build(BuildContext context) {
    centerTitle:
    true;
    return Scaffold(
        appBar: AppBar(
          title: Text('Radio Button'),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              children: [
                Radio<int>(
                  value: 1,
                  groupValue: _value,
                  onChanged: (value) {
                    setState(() {
                      _value = value as int;
                    });
                  },
                ),
                SizedBox(
                  width: 10.0,
                ),
                Text('Radio 1'),
              ],
            ),
            Row(
              children: [
                Radio<int>(
                  value: 2,
                  groupValue: _value,
                  onChanged: (value) {
                    setState(() {
                      _value = value as int;
                    });
                  },
                ),
                SizedBox(
                  width: 10.0,
                ),
                Text('Radio 2'),
              ],
            ),
            Row(
              children: [
                Radio<int>(
                  value: 3,
                  groupValue: _value,
                  onChanged: (value) {
                    setState(() {
                      _value = value as int;
                    });
                  },
                ),
                SizedBox(
                  width: 10.0,
                ),
                Text('Radio 3'),
              ],
            )
          ],
        ));
  }
}
